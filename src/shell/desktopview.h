/*
 *  Copyright 2013 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DESKTOVIEW_H
#define DESKTOVIEW_H


#include <plasmaquickview.h>
#include "panelconfigview.h"
#include <QtCore/qpointer.h>

class ShellCorona;

class DesktopView : public PlasmaQuickView
{
    Q_OBJECT
    Q_PROPERTY(bool stayBehind READ stayBehind WRITE setStayBehind NOTIFY stayBehindChanged)
    Q_PROPERTY(bool fillScreen READ fillScreen WRITE setFillScreen NOTIFY fillScreenChanged)

public:
    explicit DesktopView(ShellCorona *corona, QScreen *screen);
    virtual ~DesktopView();

    bool stayBehind() const;
    void setStayBehind(bool stayBehind);

    bool fillScreen() const;
    void setFillScreen(bool fillScreen);

    void setDashboardShown(bool shown);

protected Q_SLOTS:
    /**
     * It will be called when the configuration is requested
     */
    virtual void showConfigurationInterface(Plasma::Applet *applet);

Q_SIGNALS:
    void stayBehindChanged();
    void fillScreenChanged();

private:
    QPointer<ConfigView> m_configView;
    bool m_stayBehind : 1;
    bool m_fillScreen : 1;
};

#endif // DESKTOVIEW_H
